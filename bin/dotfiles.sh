#!/bin/sh

# DOTFILES
# -----------------

mkdir ~/.emacs.d
ln -s ~/.dotfiles/configs/emacs.d/* ~/.emacs.d
ln -s ~/.dotfiles/configs/profile ~/.profile
ln -s ~/.dotfiles/configs/zshrc ~/.zshrc
ln -s ~/.dotfiles/configs/tmux ~/.tmux.conf
